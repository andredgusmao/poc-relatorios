package br.com.redhat.fuse.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class RotaRelatorios extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("smb://{{relatorios.smb.username}}:{{relatorios.smb.password}}@{{relatorios.smb.path}}?noop=true&idempotentKey=${file:name}-${file:modified}")
			.routeId("relatorios-samba")
			.to("file://{{relatorios.pvc.path}}");
		
		from("file://{{relatorios.pvc.path}}?noop=true&idempotentKey=${file:name}-${file:modified}")
			.routeId("relatorios-pvc")
			.log("Recebido ${file:name}");
	}

}
